import numpy as np
import cv2
import torch
import glob as glob

from model import create_model
from config import DEVICE, NUM_CLASSES, OUT_DIR, MODEL_NAME, NUM_EPOCHS
from config import CLASSES, DETECTION_THRESHOLD, VAL_DIR

model = create_model(NUM_CLASSES, load_from=f"{OUT_DIR}/{MODEL_NAME}{NUM_EPOCHS}.pth")
model.eval()

test_images = glob.glob(f"{VAL_DIR}/*.png")



for i in range(len(test_images)):
    image_name = test_images[i].split('/')[-1].split('.')[0]
    image = cv2.imread(test_images[i])
    orig_image = image.copy()

    image = cv2.cvtColor(orig_image, cv2.COLOR_BGR2RGB).astype(np.float32)

    image /= 255.0

    image = np.transpose(image, (2, 0, 1)).astype(np.float)

    image = torch.tensor(image, dtype=torch.float)

    image = torch.unsqueeze(image, 0)
    with torch.no_grad():
        outputs = model(image)

    outputs = [{k: v.to('cpu') for k, v in t.items()} for t in outputs]

    if len(outputs[0]['boxes']) != 0:
        boxes = outputs[0]['boxes'].data.numpy()
        scores = outputs[0]['scores'].data.numpy()
        # filter out boxes according to `detection_threshold`
        boxes = boxes[scores >= DETECTION_THRESHOLD].astype(np.int32)
        draw_boxes = boxes.copy()
        pred_classes = [CLASSES[i] for i in outputs[0]['labels'].cpu().numpy()]

        for j, box in enumerate(draw_boxes):
            cv2.rectangle(orig_image,
                        (int(box[0]), int(box[1])),
                        (int(box[2]), int(box[3])),
                        (0, 0, 255), 2)
            cv2.putText(orig_image, pred_classes[j], 
                        (int(box[0]), int(box[1]-5)),
                        cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 255, 0), 
                        2, lineType=cv2.LINE_AA)

        cv2.imwrite(f"{OUT_DIR}/test_predictions/{image_name}.png", orig_image,)

    print(f"Image {i+1} done...")
    print('-'*50)

print('TEST PREDICTIONS COMPLETE')
        