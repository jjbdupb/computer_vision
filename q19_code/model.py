import torchvision
import torch
from torchvision.models.detection.faster_rcnn import FastRCNNPredictor


def create_model(num_classes, pretrained=True, load_from=None):
    
    # load Faster RCNN pre-trained model
    model = torchvision.models.detection.fasterrcnn_resnet50_fpn(pretrained=pretrained)
    
    # get the number of input features 
    in_features = model.roi_heads.box_predictor.cls_score.in_features
    # define a new head for the detector with required number of classes
    model.roi_heads.box_predictor = FastRCNNPredictor(in_features, num_classes)
    if load_from is not None:
        model.load_state_dict(torch.load(load_from))
    return model