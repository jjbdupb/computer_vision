import torch

BATCH_SIZE = 2
RESIZE_TO = 512
NUM_EPOCHS = 2
DEVICE = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')

TRAIN_DIR = "DATA/val"
VAL_DIR = "DATA/train"

# classes

CLASSES = ['background', 'with_mask', 'without_mask', 'mask_weared_incorrect']
NUM_CLASSES = len(CLASSES)

OUT_DIR = 'OUTPUTS'
MODEL_NAME = 'face_detect_2.0'
DETECTION_THRESHOLD = 0.6

SAVE_PLOTS_EPOCH = 2
SAVE_MODEL_EPOCH = 2


